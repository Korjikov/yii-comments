<?php

use yii\db\Migration;

/**
 * Class m181216_080557_tables
 */
class m181216_080557_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181216_080557_tables cannot be reverted.\n";
        
        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('com', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(255)->notNull()->unique(),
            'text'          => $this->text(),
            'modified_at'   => $this->string(),
            'modified_by'   => $this->string(255),
            'parent'        => $this->integer()->defaultValue(0),
        ]);
    }

    public function down()
    {
        echo "m181216_080557_tables cannot be reverted.\n";
        
        $this->dropTable('com');
        
        return false;
    }
}
