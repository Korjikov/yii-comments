<?php

namespace sat\comments\controllers;

use yii\web\Controller;

/**
 * Default controller for the `comments` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionNew()
    {
		$object =  $this->render('new');
    	$object = $object . "<script>console.log( 'Controller debug' );</script>";
        return $object;
    }
}
