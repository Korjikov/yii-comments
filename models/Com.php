<?php

namespace sat\comments\models;

use Yii;

/**
 * This is the model class for table "com".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $modified_at
 * @property string $modified_by
 * @property int $parent
 */
class Com extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'com';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['modified_at'], 'safe'],
            [['modified_at'], 'default' /*, 'value'=>date("Y-m-d H:m:s")*/],
            [['parent'], 'default', 'value' => null],
            [['parent'], 'integer'],
            [['title', 'modified_by'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'modified_at' => 'Modified At',
            'modified_by' => 'Modified By',
            'parent' => 'Parent',
        ];
    }
}
