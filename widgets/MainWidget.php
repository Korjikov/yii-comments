<?php

namespace sat\comments\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use sat\comments\models\Com;
use sat\comments\models\ComSearch;
use yii\grid\GridView;

class MainWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $searchModel = new ComSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $this->message = GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'text:ntext',
            'modified_at',
            'modified_by',
            //'parent',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    
        }
    }

    public function run()
    {
        return $this->message;
        //return Html::encode($this->message);
    }
}
