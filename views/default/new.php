<div class="comments-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
    <div class="row">
        <div class="col-md-10">
            <?php
            use yii\jui\DatePicker;
            ?>
            <?= DatePicker::widget([
                'attribute' => 'from_date',
                'language' => 'ru',
                'clientOptions' => [
                    'dateFormat' => 'yy-mm-dd',
                ],
            ]) ?>
            
        </div>
        <div class="col-md-2">
            <?php //Yii::$app->session->setFlash('error', ['Error 1']); ?>
            <?php //Yii::$app->session->setFlash('success', [var_dump(Yii::$app->user,1)]); ?>
            <script>console.log( '<?php echo serialize(Yii::$app->user); ?>' );</script>
        </div>
    </div>

</div>

<script>console.log( 'View debug' );</script>