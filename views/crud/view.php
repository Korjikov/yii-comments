<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model sat\comments\models\Com */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Coms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="com-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text:ntext',
            'modified_at',
            'modified_by',
            'parent',
        ],
    ]) ?>

</div>

