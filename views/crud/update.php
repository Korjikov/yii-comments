<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model sat\comments\models\Com */

$this->title = 'Update Com: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Coms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="com-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

