<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel sat\comments\models\ComSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="com-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Com', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'text:ntext',
            'modified_at',
            'modified_by',
            //'parent',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

