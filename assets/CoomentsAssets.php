<?php

namespace sat\comments\assets;

use yii\web\AssetBundle;

class CommentsAsset extends AssetBundle
{
	
	/**
	 * @inherit
	 */
	public $sourcePath = '@vendor/sat/yii2-comments/assets/css';

	/**
	 * @inherit
	 */
	public $css = array(
		'css/comments.css',
	);

	/**
	 * @inherit
	 */
	public $depends = [
    ];

}
