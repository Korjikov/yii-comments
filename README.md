Yii2 Comments
=============

This module provide a comments managing system for Yii2 application.


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --dev sat/yii2-comments dev-master
```
or
```
composer require --dev sat/yii2-comments dev-master
```

or add

```json
"sat/yii2-comments": "*"
```

to the require section of your composer.json.


Configuration
-----------------------

**Database Migrations**

Before using Comments Widget, we'll also need to prepare the database.
```php
php yii migrate --migrationPath=@vendor/yii2mod/yii2-satcomments/migrations
```
**Module setup**

To access the module, you need to add the following code to your application configuration:
```php
'modules' => [
    'comment' => [
        'class' => 'sat\comments\Module',
        // when admin can edit comments on frontend
        'enableInlineEdit' => false,
    ],
]
```

**Widget setup**

```php
echo \sat\comments\widgets\MainWidget::widget();
```
